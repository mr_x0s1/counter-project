import { useState } from 'react';
import './App.css'

function App() {

  const [counter_val, setCounter_val] = useState(5)

  const increaseValue = () => {
    if (counter_val < 20) {
      setCounter_val(counter_val + 1)
    }
  }

  const decreaseValue = () => {
    if (counter_val > 0) {
      setCounter_val(counter_val - 1)
    }
  }

  return (
    <>
      <h2>Counter Project | @lokYadav</h2>
      <h4>Value : {counter_val}</h4>
      <button
      onClick={increaseValue}
      >Increase</button>
      <br /> <br />
      <button
      onClick={decreaseValue}
      >Decrease</button>
    </>
  )
}

export default App
